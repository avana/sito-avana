# -*- coding: utf-8 -*-
import hashlib
import uuid
from datetime import datetime, date, timezone, timedelta
from typing import Optional

import click
import pkg_resources
from lektor.build_programs import BuildProgram
from lektor.db import F
from lektor.environment import Expression
from lektor.pluginsystem import Plugin
from lektor.context import get_ctx, url_to
from lektor.sourceobj import VirtualSourceObject
from lektor.utils import build_url

from markupsafe import escape

class Calendar:
    """A helper class that creates ICS calendars.

    :param title: the title of the feed. Required.
    :param title_type: the type attribute for the title element.  One of
                       ``'html'``, ``'text'`` or ``'xhtml'``.
    :param url: the url for the feed (not the url *of* the feed)
    :param id: a globally unique id for the feed.  Must be an URI.  If
               not present the `feed_url` is used, but one of both is
               required.
    :param updated: the time the feed was modified the last time.  Must
                    be a :class:`datetime.datetime` object.  If not
                    present the latest entry's `updated` is used.
                    Treated as UTC if naive datetime.
    :param feed_url: the URL to the feed.  Should be the URL that was
                     requested.
    :param entries: a list with the entries for the feed. Entries can also
                    be added later with :meth:`add`.

    Everywhere where a list is demanded, any iterable can be used.
    """

    default_generator = ("Werkzeug", None, None)

    def __init__(self, title=None, entries=None, **kwargs):
        self.title = title
        self.title_type = kwargs.get("title_type", "text")
        self.url = kwargs.get("url")
        self.feed_url = kwargs.get("feed_url", self.url)
        self.id = kwargs.get("id", self.feed_url)
        self.updated = kwargs.get("updated")
        self.rights_type = kwargs.get("rights_type")
        self.generator = kwargs.get("generator")
        if self.generator is None:
            self.generator = self.default_generator
        self.entries = list(entries) if entries else []


        if not self.title:
            raise ValueError("title is required")
        if not self.id:
            raise ValueError("id is required")

    def add(self, *args, **kwargs):
        """Add a new entry to the feed.  This function can either be called
        with a :class:`Event` or some keyword and positional arguments
        that are forwarded to the :class:`Event` constructor.
        """
        if len(args) == 1 and not kwargs and isinstance(args[0], Event):
            self.entries.append(args[0])
        else:
            kwargs["feed_url"] = self.feed_url
            self.entries.append(Event(*args, **kwargs))

    def __repr__(self):
        return "<%s %r (%d entries)>" % (
            self.__class__.__name__,
            self.title,
            len(self.entries),
        )

    def generate(self):
        """Return a generator that yields pieces of XML."""

        if not self.updated:
            dates = sorted([entry.updated for entry in self.entries])
            self.updated = dates[-1] if dates else datetime.utcnow()

        yield "BEGIN:VCALENDAR"
        for entry in self.entries:
            for line in entry.generate():
                yield line
        yield "END:VCALENDAR"

    def to_string(self):
        """Convert the feed into a string."""
        return "\n".join(self.generate()) + '\n'

    def get_response(self):
        """Return a response object for the feed."""
        return BaseResponse(self.to_string(), mimetype="text/calendar")

    def __call__(self, environ, start_response):
        """Use the class as WSGI response object."""
        return self.get_response()(environ, start_response)

    def __str__(self):
        return self.to_string()

class Event(object):

    """Represents a single entry in a feed.

    :param title: the title of the entry. Required.
    :param content: the content of the entry.
    :param summary: a summary of the entry's content.
    :param url: the url for the entry.
    :param id: a globally unique id for the entry.  Must be an URI.  If
               not present the URL is used, but one of both is required.
    :param updated: the time the entry was modified the last time.  Must
                    be a :class:`datetime.datetime` object.  Treated as
                    UTC if naive datetime. Required.
    :param published: the time the entry was initially published.  Must
                      be a :class:`datetime.datetime` object.  Treated as
                      UTC if naive datetime.
    :param categories: categories for the entry. Must be a list of dictionaries
                       with term (required), scheme and label (all optional)
    :param xml_base: The xml base (url) for this feed item.  If not provided
                     it will default to the item url.

    For more information on the elements see
    http://www.atomenabled.org/developers/syndication/

    Everywhere where a list is demanded, any iterable can be used.
    """

    def __init__(self, title=None, content=None, feed_url=None, **kwargs):
        self.title = title
        self.title_type = kwargs.get("title_type", "text")
        self.content = content
        self.content_type = kwargs.get("content_type", "html")
        self.url = kwargs.get("url")
        self.id = kwargs.get("id", self.url)
        self.updated = kwargs.get("updated")
        self.summary = kwargs.get("summary")
        self.summary_type = kwargs.get("summary_type", "html")
        self.published = kwargs.get("published")
        self.categories = kwargs.get("categories", [])
        self.xml_base = kwargs.get("xml_base", feed_url)

        if not self.title:
            raise ValueError("title is required")
        if not self.id:
            raise ValueError("id is required")
        if not self.updated:
            raise ValueError("updated is required")

    def __repr__(self):
        return "<%s %r>" % (self.__class__.__name__, self.title)

    def generate(self):
        """Yields pieces of ICS"""
        base = ""
        date_str = self.updated.astimezone(timezone.utc).strftime('%Y%m%dT%H%M%SZ')
        yield "BEGIN:VEVENT"
        yield f"UID:{self.url}"
        yield f"SUMMARY:{self.title}"
        yield f"DTSTAMP:{date_str}"
        yield f"DTSTART:{date_str}"
        yield f"DURATION:PT2H"
        yield f"URL:{self.url}"
        yield "END:VEVENT"

    def to_string(self):
        """Convert the feed item into a unicode object."""
        return "\n".join(self.generate()) + '\n'

    def __str__(self):
        return self.to_string()


class EventSource(VirtualSourceObject):
    def __init__(self, parent, feed_id, plugin):
        VirtualSourceObject.__init__(self, parent)
        self.plugin = plugin
        self.feed_id = feed_id

    @property
    def path(self):
        return self.parent.path + '@ics/' + self.feed_id

    @property
    def url_path(self):
        p = self.plugin.get_feed_config(self.feed_id, 'url_path')
        if p:
            return p

        return build_url([self.parent.url_path, self.filename])

    def __getattr__(self, item):
        try:
            return self.plugin.get_feed_config(self.feed_id, item)
        except KeyError:
            raise AttributeError(item)

    @property
    def feed_name(self):
        return self.plugin.get_feed_config(self.feed_id, 'name') \
            or self.feed_id

    @property
    def max_days_past(self) -> Optional[int]:
        try:
            raw_value = int(self.plugin.get_feed_config(self.feed_id, 'max_days_past'))
        except KeyError:
            raw_value = -1

        return raw_value if raw_value > 0 else None


def get(item, field, default=None):
    if field in item:
        return item[field]
    return default


def get_id(s):
    return uuid.UUID(
        bytes=hashlib.md5(s.encode('utf-8')).digest(), version=3).urn


def get_item_title(item, field):
    if field in item:
        return item[field]
    return item.record_label


def get_item_body(item, field):
    if field not in item:
        raise RuntimeError('Body field %r not found in %r' % (field, item))
    with get_ctx().changed_base_url(item.url_path):
        return str(escape(item[field]))


def get_item_updated(item, field):
    if field in item:
        rv = item[field]
    else:
        rv = datetime.utcnow()
    if isinstance(rv, date) and not isinstance(rv, datetime):
        rv = datetime(*rv.timetuple()[:3])
    return rv


class ICSBuilderProgram(BuildProgram):
    def produce_artifacts(self):
        self.declare_artifact(
            self.source.url_path,
            sources=list(self.source.iter_source_filenames()))

    def build_artifact(self, artifact):
        ctx = get_ctx()
        blog = self.source.parent

        summary = get(blog, self.source.blog_summary_field) or ''
        if hasattr(summary, '__html__'):
            subtitle_type = 'html'
            summary = str(summary.__html__())
        else:
            subtitle_type = 'text'
        generator = ('Lektor ICS Plugin',
                     'https://github.com/t73fde/lektor-feed',
                     pkg_resources.get_distribution('lektor-feed').version)

        project_id = ctx.env.load_config().base_url
        if not project_id:
            project_id = ctx.env.project.id
        feed = Calendar(
            title=self.source.feed_name,
            feed_url=url_to(self.source, external=True),
            url=url_to(blog, external=True),
            id=get_id(project_id),
            generator=generator)

        if self.source.items:
            # "self.source.items" is a string like "site.query('/blog')".
            expr = Expression(ctx.env, self.source.items)
            items = expr.evaluate(ctx.pad)
        else:
            items = blog.children

        if self.source.item_model:
            items = items.filter(F._model == self.source.item_model)

        order_by = '-' + self.source.item_date_field
        items = items.order_by(order_by).limit(int(self.source.limit))

        for item in items:
            event_date = get_item_updated(
                        item, self.source.item_date_field)
            if self.source.max_days_past is not None:
                now = datetime.utcnow().replace(tzinfo=timezone.utc)
                if event_date < (now - timedelta(days=self.source.max_days_past)):
                    continue
            try:
                feed.add(
                    get_item_title(item, self.source.item_title_field),
                    get_item_body(item, self.source.item_body_field),
                    xml_base=url_to(item, external=True),
                    url=url_to(item, external=True),
                    content_type='html',
                    id=get_id(u'%s/%s' % (
                        ctx.env.project.id,
                        item['_path'].encode('utf-8'))),
                    updated=event_date)
            except Exception as exc:
                msg = '%s: %s' % (item['_id'], exc)
                click.echo(click.style('E', fg='red') + ' ' + msg)

        with artifact.open('wb') as f:
            f.write(feed.to_string().encode('utf-8'))


class ICSPlugin(Plugin):
    name = u'Lektor Calendar plugin'
    description = u'Lektor plugin that generates ICS calendars.'

    defaults = {
        'source_path': '/',
        'name': None,
        'url_path': None,
        'filename': 'calendar.ics',
        'blog_summary_field': 'summary',
        'items': None,
        'limit': 50,
        'item_title_field': 'title',
        'item_body_field': 'body',
        'item_date_field': 'event_date',
        'item_model': None,
        'max_days_past': -1,
    }

    def get_feed_config(self, feed_id, key):
        default_value = self.defaults[key]
        return self.get_config().get('%s.%s' % (feed_id, key), default_value)

    def on_setup_env(self, **extra):
        self.env.add_build_program(EventSource, ICSBuilderProgram)

        @self.env.virtualpathresolver('ics')
        def feed_path_resolver(node, pieces):
            if len(pieces) != 1:
                return

            _id = pieces[0]

            config = self.get_config()
            if _id not in config.sections():
                return

            source_path = self.get_feed_config(_id, 'source_path')
            if node.path == source_path:
                return EventSource(node, _id, plugin=self)

        @self.env.generator
        def generate_feeds(source):
            for _id in self.get_config().sections():
                if source.path == self.get_feed_config(_id, 'source_path'):
                    yield EventSource(source, _id, self)
