# -*- coding: utf-8 -*-
import datetime

from lektor.pluginsystem import Plugin


class TodayPlugin(Plugin):
    name = 'today'
    description = u'Porcoddio le funzioni base'

    def on_setup_env(self, **extra):
        utc =  datetime.timezone.utc
        self.env.jinja_env.globals['today'] = datetime.date.today()
        self.env.jinja_env.globals['now'] = datetime.datetime.now(utc)
