_model: blog-post
---
title: UN DOS TRES: passo passo oltre le Big Tech
---
pub_date: 2025-02-20
---
cover: /2025-un-dos-tres/bigtech.jpg
---
body:

Probabilmente avete già visto [questa foto](/2025-un-dos-tres/bigtech.jpg).

Qualunque cosa pensiate del _passato_ di queste persone, delle aziende che hanno dietro, dei prodotti che creano e dei loro allineamenti politici pensiamo che sul presente non ci sia tanto da discutere: sono un gruppo di miliardari della destra ultraliberista che usa i prodotti che controlla per promuovere la propria agenda politica.

Questo, purtroppo, ci riguarda direttamente: le persone che decidono di usare i social media commerciali per mettersi in contatto con le realtà di movimento vengono esposte alla disinformazione e alla violenza fascista;  le nostre mobilitazioni in solidarietà con la Palestina sono catturate all'interno di un sistema che collabora attivamente con il genocidio in corso. Questo è spesso visto come il contraltare di una "grande capacità di comunicazione". Peccato che non sia vero: il meccanismo dei social, tra le altre cose, non permette di raggiungere persone che non siano già nella tua cerchia di relazioni.

Per poter invertire la tendenza, è necessario capire come ci troviamo qui. Uno dei meccanisi significativi è sicuramente quello della massa critica: i social media commerciali ci sono apparsi come l'unico luogo possibile in cui fare comunicazione digitale. Se tuttə sono lì, ci sto anche io... è un circolo vizioso.

Spezzarlo è possibile: possiamo fare in modo che non sia necessario, per le persone interessate ai nostri contenuti, usare i social media commerciali. Pubblichiamo le nostre iniziative, le nostre riflessioni, ciò che di importante abbiamo da comunicare sul Web, se vogliamo che siano davvero accessibili. Il Web non è un mondo perfetto, ma offre margini di autonomia enormemente più alti: sui nostri siti possiamo offrire i contenuti che vogliamo e sapere che non verranno censurati, che potranno essere ricercati in futuro, che saranno accessibili a chiunque.

Parlare è facile, lo sappiamo, e la realtà spesso complicata: sui social media commerciali si è stratificato molto, ed è difficile cambiare; le conoscenze riguardo all'uso di strumenti appropriati si sono via via ridotte.
Come AvANa vogliamo dare quindi la disponibilità alle realtà di movimento romane ad un supporto pratico. Se
avete deciso che volete portare i vostri contenuti in spazi liberi dalla manipolazione, scriveteci un'email ad
[avana-prog-forte@autistici.org](mailto:avana-prog-forte@autistici.org), così ci possiamo organizzare per
vederci un pomeriggio e mettere in piedi quel che serve. Promettiamo che non serve una laurea in ingegneria,
ma solo un po' di voglia di conoscere e imparare.

> "incominciando a gustare un po’ di libertà si finisce col volerla tutta"

[avana-prog-forte@autistici.org](mailto:avana-prog-forte@autistici.org)

**U**nità<br/>
**N**avigante<br/>
per la<br/>
**D**iminuzione<br/>
dell'**O**nnipotenza<br/>
della **S**iliconvalley<br/>
<br/>
**T**eam<br/>
**R**apido<br/>
**E**liminazione<br/>
**S**ocial


