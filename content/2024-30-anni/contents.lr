_model: event
---
title: 30 anni di AvANa
---
pub_date: 2024-09-04
---
event_date: 2024-12-20 18:00:00 Europe/Rome
---
event_place: forte
---
cover: /2024-30-anni/cover.jpg
---
body:

Due giorni di festeggiamenti per i 30 anni di AvANa.

Programma:

### Venerdì 20 dicembre

Dalle 21, workshop:

 - scacchi online e sistemi di anticheating
 - digital vinyl system, workshop pratico di utilizzo di software libero per scratchare. Il workshop
   degenererà in djset di chiusura!

### Sabato 21 dicembre

Si inizia dalle 14:30 con 100 CELLENGE: una caccia al tesoro nelle celle del forte!
La caccia al tesoro dura fino alle 19!

Dopo cena, evento di gala conclusivo della caccia al tesoro. A seguire, concerto con:

 - Cobol
 - Dj Set di UAP a seguire

### Accessibilità

 - la caccia al tesoro di sabato si svolgerà tutta nelle "celle" del forte, ovvero un luogo seminterrato. Il luogo è
   raggiungibile per persone con ridotta mobilità, ma il pavimento è leggermente sconnesso, quindi potrebbe
   essere necessaria l'assistenza di una persona terza. I corridoi interni delle celle sono abbastanza
   stretti, ma comunque sufficienti da permettere il passaggio comodo di una sedia a rotelle.
   Le celle possono avere un aspetto leggermente claustrofobico e sono un po' buie. La maggior parte delle
   stanze è dotata di piccole feritoie verso l'esterno.

 - il resto del programma si svolgerà invece in altri ambienti del CSOA Forte Prenestino. Raggiungere il forte
   con la sedia a rotelle non è facile, a meno di arrivare con la macchina fino all'ingresso: la strada è
   sterrata e con parecchie buche.

### Ospitalità

Se venite da fuori Roma e vi serve ospitalità, [scriveteci](/about/)!

Il dormitorio è un luogo fresco e ben ventilato, portatevi un buon sacco a pelo.

## Acta

Qui i [manuali](/upload/2024-caccia-tesoro/man/) della caccia al tesoro:
 - [avana30(6)](/upload/2024-caccia-tesoro/man/avana30.6.html)
 - [compleanno-sera(6)](/upload/2024-caccia-tesoro/man/compleanno-sera.6.html)

### Gran galà

#### Squadra 1

 - Tema: saldatura
 - Parola: impronta digitale
 - Lingua: napoletano

Testo:

    O' saldatore o' saldatore
    Ogni saldatore è bello a mamma soja
    
    ho scoperto che a saldare
    forse è meglio non toccare
    
    se sei brava elettricista
    saldi pure a prima vista
    a cannella a stagno o a TIG
    
    O' saldatore o' saldatore
    Ogni saldatore è bello a mamma soja
    
    Accidenti a sta scintilla
    sto sempre a faticà
    Questa maschera è na vergogna
    pare uscita da una fogna
    e non smetto di sudà
    
    Oggi è sabato è il compleanno di avana
    oggi è sabato
    m'agg appicciat' a man'
    oggi ho perso
    un'impronta digitale
    oggi è sabato
    e non si può saldare!

#### Squadra 3


 - Tema: licenze libere
 - Parola: ragnatela
 - Lingua: francese
 - Dissing: forbici spuntate
 - Brano: YMCA

    Taxi
    Balneari, tabacchi e
    Taxi
    Ambulanti, baracche e
    Taxi
    Videlottery, gambling e
    forbici affilate

    Cosa vogliamo
    noi vogliamo le
    license free
    Nous volulons le
    license free

    I monopoli di stato
    sono una ragnatela
    aboliamo le licenze stasera!


#### Squadra 4

 - Tema: GPT
 - Parola: cacciavite

**Vincitrice**

    Se il mondo ti confonde
    non lo capisci più
    Se nulla ti soddisfa
    ti annoi sempre più
    
    scienziati ed ingegneri
    hanno inventato già
    una generativa in chiacchiere robot
    
    c'è questa strana chat
    vedrai ti piacerà
    Rispnde alle domande
    e alle curiosità
    
    È come un cacciavite
    ma non si rompe mai
    consuma la corrente
    finché ci estinguerà
    oooooh gpt robot!
    oooooh gpt robot!
    
    Io ti sfamo, io ti cresco, io ti nutro
    GPT Robot
    
    Ha dentro una nvidia
    e quanto scalda già
    un processore video
    che ci distruggerà
    lei lavora duro
    tu povero sarai
    di plastica e di acciaio
    e non si fredda mai
    
    c'è questa strana chat
    vedrai ti piacerà
    risponde alle domande
    e alle curiosità
    ha un cuore di silicio
    che non si ferma mai
    consuma la corrente finché ci estinguerà
    
    Ooooh GPT ROBOT
    Ooooh GPT ROBOT
    Io ti sfamo
    io ti cresco
    io ti nutro
    GPT ROBOT
    
    Elon got a job
    with the help of a friend
    baby baby baby
    Elon got a job
    with the help of a friend
    baby baby baby
    
    Ooooh GPT ROBOT
    Ooooh GPT ROBOT
    Io ti sfamo
    io ti cresco
    io ti nutro
    GPT ROBOT



#### AvANa - fuori concorso

    stessa storia stesso forte stesso hacklab
    stessa gente che sta sempre seduta a salda'
    non lo so che faccio qui
    mi loggo un po'... e vedo un flame in lista che mi
    guarda e sembra chiedermi a chi scriviamo noi
    
    **RITORNELLO:**
    gli anni d'oro tutti su xchat
    gli anni delle ircwars e dei port map
    gli anni delle immense compagnie
    gli anni delle query sempre in due
    gli anni d'oro dei server di irc
    gli anni di mirc e dei plugins
    gli anni se la stanza lascerai
    gli anni non la ritroverai maaaaaiiii   non la ritroverai maaai
    
    stesso posto stesso server stessa chat
    vedo un nick che conosco c'avra' la mia eta'
    come va? saluta? no!
    cosi' io faccio un whois ed e` idle assai
    lui sta su signal mentre io sto ancora quaaa
    
    **RIT.**
    
    stesso posto stesso server stessa chat
    sta chiudendo il server poi me ne andro' su whatsapp
    solo bot davanti a me
    Cosa vuoi? Il tempo passa per tutti lo sai
    Nessuno indietro lo riporterà, neppure noi
    
    **RIT.**

