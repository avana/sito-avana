# Sito AvANa

## How To

### Vedere il Sito

Installati `lektor`

`lektor serve`

### Aggiungi un evento

Dentro a `content/` crea una nuova cartella, e chiamala `ANNO-cosa-facciamo`, tipo `2022-montiamo-asciugacapelli`.
Dentro mettici un file che si chiama `contents.lr` . Per il contenuto, prendi ispirazione da quello di un altro eventi precedente. Dopo il preambolo coi metadati, il resto è markdown.

Se è al forte, tutto ok. Se è in un altro posto, potresti doverlo aggiungere  a `databags/places.ini`.

Per aggiungere un'immagine all'evento, mettila nella dir dell'evento col nome di cover.png
Poi aggiungi il metadato "cover" a /ANNO-cosa-facciamo/cover.png
Vedi content/2023-trasmettitore-fm/contents.lr per un esempio

### Mettere online

C'è la continous integration!

Se pushi su una branch diversa da master, vedi il sito nuovo su
https://avana.forteprenestino.net/staging/

Se pushi su master, vedi tutto sul sito vero. Occhio che c'è la cache!

